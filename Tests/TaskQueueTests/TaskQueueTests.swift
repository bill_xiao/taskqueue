    import XCTest
    @testable import TaskQueue

    final class TaskQueueTests: XCTestCase {
        var queue = TaskQueue()
        func testExample() {
            // This is an example of a functional test case.
            // Use XCTAssert and related functions to verify your tests produce the correct
            // results.
            queue.tasks += {
                // update the UI
                XCTAssertEqual("Hello!", "Hello!")
            }
            queue.run()
            
        }
    }
